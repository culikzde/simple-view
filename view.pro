TEMPLATE = aux

SOURCES *= view.py window.py util.py edit.py info.py tree.py prop.py small.py tools.py grep.py options.py
SOURCES *= tools.ini README.md
SOURCES *= tutorial/tutorial_plugin.py

SOURCES *= tutorial/mini-parser/mini_lexer.py
SOURCES *= tutorial/mini-parser/mini_parser.py

SOURCES *= tutorial/small-grammar/small_lexer.py
SOURCES *= tutorial/small-grammar/small_output.py
SOURCES *= tutorial/small-grammar/small_grammar.py
SOURCES *= tutorial/small-grammar/small_symbols.py
SOURCES *= tutorial/small-grammar/small_tohtml.py
SOURCES *= tutorial/small-grammar/small_tolout.py
SOURCES *= tutorial/small-grammar/small_view.py

SOURCES *= tutorial/simple-grammar/simple_lexer.py
SOURCES *= tutorial/simple-grammar/simple_output.py
SOURCES *= tutorial/simple-grammar/simple_grammar.py
SOURCES *= tutorial/simple-grammar/simple_symbols.py
SOURCES *= tutorial/simple-grammar/simple_tohtml.py
SOURCES *= tutorial/simple-grammar/simple_tolout.py
SOURCES *= tutorial/simple-grammar/simple_toparser.py
SOURCES *= tutorial/simple-grammar/simple_toproduct.py
SOURCES *= tutorial/simple-grammar/simple_view.py

DISTFILES *= tutorial/c1.c
DISTFILES *= tutorial/c2.c
DISTFILES *= tutorial/cecko.g
DISTFILES *= tutorial/cecko2.g
DISTFILES *= tutorial/grammar.g
DISTFILES *= tutorial/sim.g
DISTFILES *= tutorial/mini_input.t



