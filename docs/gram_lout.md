### Zobrazení programem LOUT

**Program Lout**

<http://jeffreykingston.id.au/lout/>  
<https://en.wikipedia.org/wiki/Lout_%28software%29>  
Manuál: <http://www.adrianjwells.freeuk.com/lout.pdf> kapitola 9.8.
Syntaktické diagramy

Mějme na vstupu gramatické pravidlo

``` C++
   if_stat  :  "if" "(" expr ")" stat ( "else" stat  )? ;
```

Program *plain\_tolout.py*
<http://gitlab.fjfi.cvut.cz/culikzde/view/-/blob/master/tutorial/plain-grammar/plain_tolout.py>  
prochází gramatiku uloženou ve stromu a  
vytvoří následující textový výstup

    @SyntaxDiag
    title {if_stat}
    {
       @StartRight
       @Sequence
       A {blue @Color @BCell "if"}
       B {blue @Color @BCell "("}
       C {@ACell "expr"}
       D {blue @Color @BCell ")"}
       E {@ACell "stat"}
       F {@Optional
             @Sequence
             A {blue @Color @BCell "else"}
             B {@ACell "stat"}
       }
    }

Tento text je vstupem pro program Lout.  
Lout vytvoří výstup v PostScriptu :
<http://kmlinux.fjfi.cvut.cz/~culikzde/sos/gram.ps>
![gram.ps](/prekl//gram.ps)

![gram_lout.png](pictures/gram_lout.png)

Legendární gramatika programovacího jazyka Pascal
<http://www.cs.utexas.edu/users/novak/grammar.html>
